import { DOM_CONTROLLER } from "../controllers/dom_controllers.js";

export const DOM_MODELS = {
  addNewProductModal: () => {
    return DOM_CONTROLLER.DOM(
      "#product-brand",
      "#product-name",
      "#product-image",
      "#price",
      "#ram",
      "input[name='rom-measure-radio']",
      "#display"
    );
  },

  noticeElementArray: () => {
    return DOM_CONTROLLER.DOM(
      "#brand-notice",
      "#name-notice",
      "#image-notice",
      "#price-notice",
      "#ram-notice"
    );
  },
};
