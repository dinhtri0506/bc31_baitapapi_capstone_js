export const DisplayInput = `
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Display describe</span>
    </div>

    <textarea
    id="display"
    class="form-control"
    maxlength="50"
    aria-label="With textarea"
    ></textarea>
</div>
`;
