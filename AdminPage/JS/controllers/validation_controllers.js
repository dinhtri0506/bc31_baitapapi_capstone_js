let validate = (falseCondition, noticeElId, noticeContent) => {
  let noticeEl = document.getElementById(noticeElId);
  if (falseCondition) {
    noticeEl.innerHTML = noticeContent;
    return false;
  }
  noticeEl.innerHTML = "";
  return true;
};

export const VALIDATION_CONTROLLERS = {
  emptyValidate: (
    value,
    noticeElId,
    noticeContent = "This field cannot be blank!"
  ) => {
    let falseCondition = value.length === 0;
    return validate(falseCondition, noticeElId, noticeContent);
  },

  maxCharactersValidate: (fieldName, value, max, noticeElId) => {
    let falseCondition = value.length > max;
    let noticeContent = `Max characters of ${fieldName} is ${max}`;
    return validate(falseCondition, noticeElId, noticeContent);
  },

  positiveNumberValidate: (fieldName, value, noticeElId) => {
    let falseCondition = isNaN(value) || value * 1 < 0;
    let noticeContent = `${fieldName} must be a number and can't be less than 0`;
    return validate(falseCondition, noticeElId, noticeContent);
  },

  positiveIntegerValidate: (fieldName, value, noticeElId) => {
    let falseCondition = (value * 1) % 1 !== 0 || value * 1 < 0;
    let noticeContent = `${fieldName} must be a integer and can't be less than 0`;
    return validate(falseCondition, noticeElId, noticeContent);
  },

  productInfoValidate: (product) => {
    let productBrand = VALIDATION_CONTROLLERS.emptyValidate(
      product.brand,
      "brand-notice",
      "Make your selection!"
    );

    let productName = () => {
      if (VALIDATION_CONTROLLERS.emptyValidate(product.name, "name-notice")) {
        return VALIDATION_CONTROLLERS.maxCharactersValidate(
          "product name",
          product.name,
          24,
          "name-notice"
        );
      }
      return false;
    };
    productName();

    let productImage = VALIDATION_CONTROLLERS.emptyValidate(
      product.img,
      "image-notice"
    );

    let productPrice = VALIDATION_CONTROLLERS.positiveNumberValidate(
      "Price",
      product.price,
      "price-notice"
    );

    let productRAM = VALIDATION_CONTROLLERS.positiveIntegerValidate(
      "RAM",
      product.ram,
      "ram-notice"
    );

    return (
      productBrand &&
      productName() &&
      productImage &&
      productPrice &&
      productRAM
    );
  },
};
