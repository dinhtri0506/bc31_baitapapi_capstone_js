import { DOM_MODELS } from "../models/dom_models.js";

export const MODAL_CONTROLLERS = {
  setDataForModalForm: (product) => {
    const elementArray = DOM_MODELS.addNewProductModal();
    elementArray[0].value = product.brand;
    elementArray[1].value = product.name;
    elementArray[2].value = product.img;
    elementArray[3].value = product.price;
    elementArray[4].value = product.ram;
    document
      .querySelectorAll("input[name='rom-measure-radio']")
      .forEach((item) => {
        if (item.value === product.rom) {
          item.checked = true;
        }
      });
    elementArray[6].value = product.display;
  },
};
