export const DATA_CONTROLLERS = {
  replaceData: (data) => {
    let renderData = JSON.parse(JSON.stringify(data));
    renderData.forEach((item) => {
      item.price * 1 === 0 ? (item.price = "Coming soon") : (item.price *= 1);

      item.ram * 1 === 0 ? (item.ram = "updating...") : (item.ram += "GB");

      item.rom * 1 === 0 ? (item.rom = "updating...") : (item.rom = item.rom);

      item.display.length === 0
        ? (item.display = "updating...")
        : (item.display = item.display);
    });
    return renderData;
  },
  sortData: (data) => {
    let brandSortedArray = data;
    return brandSortedArray.sort((firstItem, secondItem) => {
      const firstBrand = firstItem.brand.toUpperCase();
      const secondBrand = secondItem.brand.toUpperCase();
      if (firstBrand < secondBrand) {
        return -1;
      }
      if (firstBrand > secondBrand) {
        return 1;
      }
      return 0;
    });
  },
};
