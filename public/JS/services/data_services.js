const BASE_URL = "https://62b07878e460b79df0469b79.mockapi.io/SampleStore";

export const DATA_SERVICES = {
  getData: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
};
